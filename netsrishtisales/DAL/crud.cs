﻿using netsrishtisales.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace netsrishtisales.DAL
{
    public class crud
    {
        SalesContext db = new SalesContext();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SalesContext"].ToString());

        public object SalesModels { get; private set; }

        public string inventoryupdate(string barcode, bool available, float? gwtr, int qtyremaining)
        {
            string message = "Inventory Successfully Updated!";
            SqlCommand cmd = new SqlCommand("update inventories set available='" + available + "', grossweightremaining='" + gwtr + "', qtyremaining='"+ qtyremaining + "' where barcode='" + barcode + "'", con);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                message = "Something went wrong!  Please try again!";
                crud cr = new crud();
                logfile lf = new logfile();
                lf.DateTime = DateTime.Now;
                lf.logmessage = ex.Message;
                lf.user = "testuser";
                lf.storeid = 1;
                db.Entry(lf).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();
            }
            finally
            {
                con.Close();
            }
            return message;
        }
        public void logfile()
        {

        }
    }
}