﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace netsrishtisales.Models
{
    public class SalesContext: DbContext
    {
        public DbSet<Inventory> inventory { get; set; }

        public DbSet<Estimate> estimate { get; set; }
        public DbSet<Sale> sale { get; set; }
        public DbSet<Customer> customer { get; set; }
        public DbSet<Gift> gift { get; set; }
        public DbSet<Discount> discount { get; set; }
        public DbSet<ProductType> producttype { get; set; }
        //public DbSet<BoxSale>   boxsale { get; set; }
        public DbSet<logfile> LogFile { get; set; }
        public DbSet<Store> store { get; set; }
    }
}