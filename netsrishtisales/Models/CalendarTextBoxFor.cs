﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace netsrishtisales.Models
{
    public static class dateextention
    {
        public static MvcHtmlString CalenderTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes = null)
        {
            var mvcHtmlString = System.Web.Mvc.Html.InputExtensions.TextBoxFor(htmlHelper, expression, htmlAttributes ?? new { @class = "text-box single-line date-picker" });
            var xDoc = XDocument.Parse(mvcHtmlString.ToHtmlString());
            var xElement = xDoc.Element("input");
            if (xElement != null)
            {
                var valueAttribute = xElement.Attribute("value");
                if (valueAttribute != null)
                {
                    valueAttribute.Value = DateTime.Parse(valueAttribute.Value).ToShortDateString();
                    if (valueAttribute.Value == "1/1/0001")
                        valueAttribute.Value = string.Empty;
                }
            }
            return new MvcHtmlString(xDoc.ToString());
        }
    }
}