﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace netsrishtisales.Models
{
    public class logfile
    {
        public Int64 id { get; set; }
        public string logmessage { get; set; }
        public string user { get; set; }
        [ForeignKey("store")]
        public int storeid { get; set; }
        public Store store { get; set; }

        public DateTime DateTime { get; set; }
    }

    public class Inventory
    {
        [Key]
        public string Barcode { get; set; }
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public float? GrossWeight { get; set; }
        public float? NetWeight { get; set; }
        public int Qty { get; set; }
        public float? UnitPrice { get; set; }
        public string ProductDescription { get; set; }
        public float? StoneCharges { get; set; }
        public string Item { get; set; }
        public DateTime TimeStamp { get; set; }
        public float? Purity { get; set; }
        public float? GrossWeightRemaining { get; set; }
        public bool available { get; set; }
        public int qtyremaining { get; set; }
        [ForeignKey("store")]
        public int storeid { get; set; }
        public Store store { get; set; }
    }

    public class Store
    {
        [Key]
        public int storeid { get; set; }
        public string unitname { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string country { get; set; }
    }

    public partial class Estimate
    {
        public int ID { get; set; }

        [ForeignKey("inventory")]
        public string Barcode { get; set; }
        public Inventory inventory { get; set; }
        [ForeignKey("customer")]
        public int CID { get; set; }

        public Customer customer { get; set; }
        public int Qty { get; set; }
        public string Item { get; set; }
        public float? BoardRate { get; set; }
        public float? Purity { get; set; }
        public float? GrossWeight { get; set; }
        public float? NetWeight { get; set; }
        public float? StoneWeight { get; set; }
        public float? UnitPrice { get; set; }
        public float TotalPrice { get; set; }
        public string HSNNO { get; set; }
        public float Taxes { get; set; }
        public float FinalPrice { get; set; }
        public DateTime DateTimeStamp { get; set; }

    }

    public partial class Estimate
    {
        public string ProductName { get; set; }
    }
    public class Sale
    {
        public int ID { get; set; }

        [ForeignKey("inventory")]
        public string Barcode { get; set; }

        public Inventory inventory { get; set; }
        [ForeignKey("customer")]
        public int CID { get; set; }
        public Customer customer { get; set; }
        public int Qty { get; set; }
        public string Item { get; set; }
        public float? BoardRate { get; set; }
        public float? Purity { get; set; }
        public float? GrossWeight { get; set; }
        public float? NetWeight { get; set; }
        public float? StoneWeight { get; set; }
        public float? UnitPrice { get; set; }
        public float TotalPrice { get; set; }
        public string HSNNO { get; set; }
        public float Taxes { get; set; }
        public float FinalPrice { get; set; }
        public DateTime DateTimeStamp { get; set; }
    }

    public class ProductType
    {
       const float dval = 0.0F;
        public int ID { get; set; }
        public float? Purity { get; set; } = dval;
        public string Item { get; set; }
        public string HSNNO { get; set; }
        public float GSTPercent { get; set; }
        public float? BoardRate { get; set; } = dval;
        public float CGST { get; set; }
        public float SGST { get; set; }
        public float UTGST { get; set; }
        public float IGST { get; set; }
    }

    public class Customer
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }

        public string GSTNO { get; set; }
        public string BankName { get; set; }
        public string AccountNo { get; set; }
        public string IFSCCode { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public DateTime DateTimeStamp { get; set; }
        public string SwiftCode { get; set; }
        public string OtherInfo { get; set; }
    }

    public class Gift
    {
        public int ID { get; set; }
        public float TotalSaleValue { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string Description { get; set; }
        public string OtherCriteria { get; set; }
    }

    public class Discount
    {
        public int ID { get; set; }
        public string ProductType { get; set; }
        public bool RoleBased { get; set; }
        public string UOM { get; set; }
        public float PercentValue { get; set; }
        public float AmountValue { get; set; }
        public DateTime DateTimeStamp { get; set; }
    }

    public class BoxSale
    {
        public int ID { get; set; }
        [ForeignKey("inventory")]
        public string Barcode { get; set; }
        public Inventory inventory { get; set; }
        public float GrossWeight { get; set; }
        public float NetWeight { get; set; }
        public float StoneWeight { get; set; }

    }

}
