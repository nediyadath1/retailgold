﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(netsrishtisales.Startup))]
namespace netsrishtisales
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
