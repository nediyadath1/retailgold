namespace netsrishtisales.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class salescontext : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.Customers",
            //    c => new
            //    {
            //        ID = c.Int(nullable: false, identity: true),
            //        Name = c.String(),
            //        Mobile = c.String(),
            //        Email = c.String(),
            //        Address = c.String(),
            //        GSTNO = c.String(),
            //        BankName = c.String(),
            //        AccountNo = c.String(),
            //        IFSCCode = c.String(),
            //        Country = c.String(),
            //        State = c.String(),
            //        City = c.String(),
            //        DateTimeStamp = c.DateTime(nullable: false),
            //        SwiftCode = c.String(),
            //        OtherInfo = c.String(),
            //    })
            //    .PrimaryKey(t => t.ID);

            //    CreateTable(
            //        "dbo.Discounts",
            //        c => new
            //            {
            //                ID = c.Int(nullable: false, identity: true),
            //                ProductType = c.String(),
            //                RoleBased = c.Boolean(nullable: false),
            //                UOM = c.String(),
            //                PercentValue = c.Single(nullable: false),
            //                AmountValue = c.Single(nullable: false),
            //                DateTimeStamp = c.DateTime(nullable: false),
            //            })
            //        .PrimaryKey(t => t.ID);

            //    CreateTable(
            //        "dbo.Estimates",
            //        c => new
            //            {
            //                ID = c.Int(nullable: false, identity: true),
            //                Barcode = c.String(maxLength: 128),
            //                CID = c.Int(nullable: false),
            //                Qty = c.Int(nullable: false),
            //                Item = c.String(),
            //                BoardRate = c.Single(),
            //                Purity = c.Single(),
            //                GrossWeight = c.Single(),
            //                NetWeight = c.Single(),
            //                StoneWeight = c.Single(),
            //                UnitPrice = c.Single(),
            //                TotalPrice = c.Single(nullable: false),
            //                HSNNO = c.String(),
            //                Taxes = c.Single(nullable: false),
            //                FinalPrice = c.Single(nullable: false),
            //                DateTimeStamp = c.DateTime(nullable: false),
            //                ProductName = c.String(),
            //            })
            //        .PrimaryKey(t => t.ID)
            //        .ForeignKey("dbo.Customers", t => t.CID, cascadeDelete: true)
            //        .ForeignKey("dbo.Inventories", t => t.Barcode)
            //        .Index(t => t.Barcode)
            //        .Index(t => t.CID);

            //    CreateTable(
            //        "dbo.Inventories",
            //        c => new
            //            {
            //                Barcode = c.String(nullable: false, maxLength: 128),
            //                ProductName = c.String(),
            //                ProductType = c.String(),
            //                GrossWeight = c.Single(),
            //                NetWeight = c.Single(),
            //                Qty = c.Int(nullable: false),
            //                UnitPrice = c.Single(),
            //                ProductDescription = c.String(),
            //                StoneCharges = c.Single(),
            //                Item = c.String(),
            //                TimeStamp = c.DateTime(nullable: false),
            //                Purity = c.Single(),
            //                GrossWeightRemaining = c.Single(),
            //                available = c.Boolean(nullable: false),
            //                qtyremaining = c.Int(nullable: false),
            //                storeid = c.Int(nullable: false),
            //            })
            //        .PrimaryKey(t => t.Barcode)
            //        .ForeignKey("dbo.Stores", t => t.storeid, cascadeDelete: true)
            //        .Index(t => t.storeid);

            //    CreateTable(
            //        "dbo.Stores",
            //        c => new
            //            {
            //                storeid = c.Int(nullable: false, identity: true),
            //                unitname = c.String(),
            //                address = c.String(),
            //                phone = c.String(),
            //                state = c.String(),
            //                city = c.String(),
            //                country = c.String(),
            //            })
            //        .PrimaryKey(t => t.storeid);

            //    CreateTable(
            //        "dbo.Gifts",
            //        c => new
            //            {
            //                ID = c.Int(nullable: false, identity: true),
            //                TotalSaleValue = c.Single(nullable: false),
            //                ExpiryDate = c.DateTime(nullable: false),
            //                Description = c.String(),
            //                OtherCriteria = c.String(),
            //            })
            //        .PrimaryKey(t => t.ID);

            //    CreateTable(
            //        "dbo.logfiles",
            //        c => new
            //            {
            //                id = c.Long(nullable: false, identity: true),
            //                logmessage = c.String(),
            //                user = c.String(),
            //                storeid = c.Int(nullable: false),
            //                DateTime = c.DateTime(nullable: false),
            //            })
            //        .PrimaryKey(t => t.id)
            //        .ForeignKey("dbo.Stores", t => t.storeid, cascadeDelete: true)
            //        .Index(t => t.storeid);

            //    CreateTable(
            //        "dbo.ProductTypes",
            //        c => new
            //            {
            //                ID = c.Int(nullable: false, identity: true),
            //                Purity = c.Single(),
            //                Item = c.String(),
            //                HSNNO = c.String(),
            //                GSTPercent = c.Single(nullable: false),
            //                BoardRate = c.Single(),
            //                CGST = c.Single(nullable: false),
            //                SGST = c.Single(nullable: false),
            //                UTGST = c.Single(nullable: false),
            //                IGST = c.Single(nullable: false),
            //            })
            //        .PrimaryKey(t => t.ID);

            //    CreateTable(
            //        "dbo.Sales",
            //        c => new
            //            {
            //                ID = c.Int(nullable: false, identity: true),
            //                Barcode = c.String(maxLength: 128),
            //                CID = c.Int(nullable: false),
            //                Qty = c.Int(nullable: false),
            //                Item = c.String(),
            //                BoardRate = c.Single(),
            //                Purity = c.Single(),
            //                GrossWeight = c.Single(),
            //                NetWeight = c.Single(),
            //                StoneWeight = c.Single(),
            //                UnitPrice = c.Single(),
            //                TotalPrice = c.Single(nullable: false),
            //                HSNNO = c.String(),
            //                Taxes = c.Single(nullable: false),
            //                FinalPrice = c.Single(nullable: false),
            //                DateTimeStamp = c.DateTime(nullable: false),
            //            })
            //        .PrimaryKey(t => t.ID)
            //        .ForeignKey("dbo.Customers", t => t.CID, cascadeDelete: true)
            //        .ForeignKey("dbo.Inventories", t => t.Barcode)
            //        .Index(t => t.Barcode)
            //        .Index(t => t.CID);

        }
        
        public override void Down()
        {
            //DropForeignKey("dbo.Sales", "Barcode", "dbo.Inventories");
            //DropForeignKey("dbo.Sales", "CID", "dbo.Customers");
            //DropForeignKey("dbo.logfiles", "storeid", "dbo.Stores");
            //DropForeignKey("dbo.Estimates", "Barcode", "dbo.Inventories");
            //DropForeignKey("dbo.Inventories", "storeid", "dbo.Stores");
            //DropForeignKey("dbo.Estimates", "CID", "dbo.Customers");
            //DropIndex("dbo.Sales", new[] { "CID" });
            //DropIndex("dbo.Sales", new[] { "Barcode" });
            //DropIndex("dbo.logfiles", new[] { "storeid" });
            //DropIndex("dbo.Inventories", new[] { "storeid" });
            //DropIndex("dbo.Estimates", new[] { "CID" });
            //DropIndex("dbo.Estimates", new[] { "Barcode" });
            //DropTable("dbo.Sales");
            //DropTable("dbo.ProductTypes");
            //DropTable("dbo.logfiles");
            //DropTable("dbo.Gifts");
            //DropTable("dbo.Stores");
            //DropTable("dbo.Inventories");
            //DropTable("dbo.Estimates");
            //DropTable("dbo.Discounts");
            //DropTable("dbo.Customers");
        }
    }
}
