﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using netsrishtisales.Models;

namespace netsrishtisales.Controllers
{
    public class ProductTypesApiController : ApiController
    {
        private SalesContext db = new SalesContext();

        // GET: api/ProductTypesApi
        public IQueryable<ProductType> Getproducttype()
        {
            return db.producttype;
        }

        public List<ProductType> Getproductlist()
        {
            List<ProductType> plist = db.producttype.ToList();
            return plist;
        }

        public ProductType GetBoardRate(string item)
        {
            ProductType pt = db.producttype.Single(e => e.Item == item);
            return pt;
        }

        // GET: api/ProductTypesApi/5
        [ResponseType(typeof(ProductType))]
        public async Task<IHttpActionResult> GetProductType(int id)
        {
            ProductType productType = await db.producttype.FindAsync(id);
            if (productType == null)
            {
                return NotFound();
            }

            return Ok(productType);
        }

        // PUT: api/ProductTypesApi/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutProductType(int id, ProductType productType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != productType.ID)
            {
                return BadRequest();
            }

            db.Entry(productType).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ProductTypesApi
        [ResponseType(typeof(ProductType))]
        public async Task<IHttpActionResult> PostProductType(ProductType productType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.producttype.Add(productType);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = productType.ID }, productType);
        }

        // DELETE: api/ProductTypesApi/5
        [ResponseType(typeof(ProductType))]
        public async Task<IHttpActionResult> DeleteProductType(int id)
        {
            ProductType productType = await db.producttype.FindAsync(id);
            if (productType == null)
            {
                return NotFound();
            }

            db.producttype.Remove(productType);
            await db.SaveChangesAsync();

            return Ok(productType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductTypeExists(int id)
        {
            return db.producttype.Count(e => e.ID == id) > 0;
        }
    }
}