﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using netsrishtisales.Models;

namespace netsrishtisales.Controllers
{
    public class SalesController : Controller
    {
        private SalesContext db = new SalesContext();

        // GET: Sales
        public async Task<ActionResult> Index()
        {
            var sale = db.sale.Include(s => s.customer).Include(s => s.inventory);
            return View(await sale.ToListAsync());
        }

        // GET: Sales/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sale sale = await db.sale.FindAsync(id);
            if (sale == null)
            {
                return HttpNotFound();
            }
            return View(sale);
        }

        // GET: Sales/Create
        public ActionResult Create()
        {
            ViewBag.CID = new SelectList(db.customer, "ID", "Name");
            ViewBag.Barcode = new SelectList(db.inventory, "Barcode", "ProductName");
            return View();
        }

        // POST: Sales/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Barcode,CID,Qty,Item,BoardRate,Purity,GrossWeight,NetWeight,StoneWeight,UnitPrice,TotalPrice,HSNNO,Taxes,FinalPrice,DateTimeStamp")] Sale sale)
        {
            if (ModelState.IsValid)
            {
                db.sale.Add(sale);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.CID = new SelectList(db.customer, "ID", "Name", sale.CID);
            ViewBag.Barcode = new SelectList(db.inventory, "Barcode", "ProductName", sale.Barcode);
            return View(sale);
        }

        // GET: Sales/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sale sale = await db.sale.FindAsync(id);
            if (sale == null)
            {
                return HttpNotFound();
            }
            ViewBag.CID = new SelectList(db.customer, "ID", "Name", sale.CID);
            ViewBag.Barcode = new SelectList(db.inventory, "Barcode", "ProductName", sale.Barcode);
            return View(sale);
        }

        // POST: Sales/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Barcode,CID,Qty,Item,BoardRate,Purity,GrossWeight,NetWeight,StoneWeight,UnitPrice,TotalPrice,HSNNO,Taxes,FinalPrice,DateTimeStamp")] Sale sale)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sale).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CID = new SelectList(db.customer, "ID", "Name", sale.CID);
            ViewBag.Barcode = new SelectList(db.inventory, "Barcode", "ProductName", sale.Barcode);
            return View(sale);
        }

        // GET: Sales/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sale sale = await db.sale.FindAsync(id);
            if (sale == null)
            {
                return HttpNotFound();
            }
            return View(sale);
        }

        // POST: Sales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Sale sale = await db.sale.FindAsync(id);
            db.sale.Remove(sale);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
