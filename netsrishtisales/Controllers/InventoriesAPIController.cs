﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using netsrishtisales.Models;

namespace netsrishtisales.Controllers
{
    public class InventoriesAPIController : ApiController
    {
        private SalesContext db = new SalesContext();

        // GET: api/InventoriesAPI
        public IQueryable<Inventory> Getinventory()
        {
            return db.inventory;
        }

        // GET: api/InventoriesAPI/5
        [ResponseType(typeof(Inventory))]
        public async Task<IHttpActionResult> GetInventory(int id)
        {
            List<Inventory> inventorylist = await db.inventory.Where(a=>a.storeid==id).ToListAsync().ConfigureAwait(false);
            if (inventorylist == null)
            {
                return NotFound();
            }

            return Ok(inventorylist);
        }

        // PUT: api/InventoriesAPI/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutInventory(string id, Inventory inventory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != inventory.Barcode)
            {
                return BadRequest();
            }

            db.Entry(inventory).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InventoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/InventoriesAPI
        [ResponseType(typeof(Inventory))]
        public async Task<IHttpActionResult> PostInventory(Inventory inventory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.inventory.Add(inventory);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (InventoryExists(inventory.Barcode))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = inventory.Barcode }, inventory);
        }

        // DELETE: api/InventoriesAPI/5
        [ResponseType(typeof(Inventory))]
        public async Task<IHttpActionResult> DeleteInventory(string id)
        {
            Inventory inventory = await db.inventory.FindAsync(id);
            if (inventory == null)
            {
                return NotFound();
            }

            db.inventory.Remove(inventory);
            await db.SaveChangesAsync();

            return Ok(inventory);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool InventoryExists(string id)
        {
            return db.inventory.Count(e => e.Barcode == id) > 0;
        }
    }
}