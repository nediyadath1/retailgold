﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using netsrishtisales.Models;
using netsrishtisales.DAL;

namespace netsrishtisales.Controllers
{
    public class EstimatesController : Controller
    {
        List<Estimate> lest = new List<Estimate>();
        private SalesContext db = new SalesContext();
        crud cr = new crud();

        public JsonResult fetchproducttype(string barcode)
        {
            string ptype = "";
            try
            {
                 ptype = db.inventory.SingleOrDefault(e => e.Barcode == barcode).ProductType;
                return Json(ptype, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                ptype = "WrongBarCode";
                return Json(ptype, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult fetchproduct()
        {
            TempData["count"] = 0;
            return View();
        }
        // GET: Estimates

        [HttpPost]
        [ActionName("fetchproduct")]
        public ActionResult fetchproductpost()
        {
            string ptype = "";
            try
            {
                if (Session["estlist"] != null)
                {
                    lest = (List<Estimate>)Session["estlist"];
                }
                Estimate est = new Estimate();
                string barcode = Request.Form["barcode"].ToString();
                Inventory inv = new Inventory();
                string bcode = "";
                try
                {
                    try
                    {
                      bcode   = lest.SingleOrDefault(e => e.Barcode == barcode).Barcode;

                    }
                    catch
                    {

                    }
                    Inventory inventory = db.inventory.Single(e => e.Barcode == bcode);
                    ptype = inventory.ProductType;
                    float? gwtrem = inventory.GrossWeight - inventory.GrossWeightRemaining;
                    if (ptype == "Box" && gwtrem == 0)
                    {
                        ViewBag.message = "This box is empty";
                        return View();
                    }
                    else if (ptype != "Box")
                    {
                        ViewBag.message = "This barcode is already added!";
                        return View();
                    }
                }
                catch
                {

                }
                inv = db.inventory.Single(e => e.Barcode == barcode);
                if (inv.available == false)
                {
                    ViewBag.message = "This barcode is not available or is picked up for another estimate already!";
                    return View();
                }
                est.Barcode = barcode;
                est.Item = inv.Item;
                est.Taxes = db.producttype.Single(e => e.Item == est.Item).GSTPercent;
                ptype = inv.ProductType;
                if (ptype == "Box")
                {
                    est.Qty = int.Parse(Request.Form["getqty"].ToString());
                }
                else
                {
                    est.Qty = inv.Qty;
                }
                est.BoardRate = db.producttype.Single(e => e.Item == inv.Item).BoardRate;
                est.DateTimeStamp = DateTime.Now;
                try
                {
                    est.UnitPrice = inv.UnitPrice;
                    est.GrossWeight = float.Parse(Request.Form["gw"].ToString());
                    est.NetWeight = inv.NetWeight;
                    est.Purity = inv.Purity;
                    est.StoneWeight = inv.GrossWeight - inv.NetWeight;
                }
                catch { }
                est.ProductName = inv.ProductName;
                est.FinalPrice = est.Item == "gold" || est.Item == "silver" ? (float)(est.NetWeight * (est.BoardRate * est.Qty + (est.NetWeight * est.BoardRate * est.Qty) * est.Taxes / 100) + inv.StoneCharges) : (float)(est.UnitPrice * est.Qty + est.UnitPrice * est.Qty * est.Taxes / 100);
                est.HSNNO = db.producttype.Single(e => e.Item == inv.Item).HSNNO;
                est.TotalPrice = est.Item == "gold" ? (float)(est.NetWeight * est.BoardRate * est.Qty) : (float)(est.UnitPrice * est.Qty);
                lest.Add(est);
                inv.available = false;
                inv.GrossWeightRemaining = inv.GrossWeight - est.GrossWeight;
                if (inv.GrossWeightRemaining > 0 && inv.qtyremaining > 0)
                    inv.available = true;
                else
                    inv.available = false;
                inv.qtyremaining = inv.Qty - est.Qty;
                db.Entry(inv).State = EntityState.Modified;
                db.SaveChanges();
                Session["estlist"] = lest;
                return View("Index", lest);
            }
            catch (Exception ex)
            {
                ViewBag.message = "This barcode does not exist!: " + ex.Message;
                return View();
            }
        }

        public ActionResult cleardata()
        {
            List<Estimate> elist = (List<Estimate>)Session["estlist"];

            foreach (Estimate est in elist)
            {
                Inventory inv = new Inventory();
                inv.Barcode = est.Barcode;
                inv.available = true;
                int qtyinstock = db.inventory.Single(e => e.Barcode == est.Barcode).qtyremaining;
                inv.qtyremaining = qtyinstock + est.Qty;
                inv.GrossWeightRemaining = inv.GrossWeightRemaining + est.GrossWeight;
                cr.inventoryupdate(inv.Barcode, inv.available, inv.GrossWeightRemaining, inv.qtyremaining);
            }
            Session.Clear();
            return RedirectToAction("fetchproduct");
        }
        public async Task<ActionResult> Index()
        {
            var estimate = db.estimate.Include(e => e.customer).Include(e => e.inventory);
            return View(await estimate.ToListAsync());
        }

        [HttpPost]
        [ActionName("Index")]
        public ActionResult IndexPost()
        {
            return View("fetchproduct");
        }

        // GET: Estimates/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estimate estimate = await db.estimate.FindAsync(id);
            if (estimate == null)
            {
                return HttpNotFound();
            }
            return View(estimate);
        }

        // GET: Estimates/Create
        public ActionResult Create()
        {
            ViewBag.CID = new SelectList(db.customer, "ID", "Name");
            ViewBag.Barcode = new SelectList(db.inventory, "Barcode", "ProductName");
            return View();
        }

        // POST: Estimates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Barcode,CID,Qty,Item,BoardRate,Purity,GrossWeight,NetWeight,StoneWeight,UnitPrice,TotalPrice,HSNNO,Taxes,FinalPrice,DateTimeStamp")] Estimate estimate)
        {
            if (ModelState.IsValid)
            {
                db.estimate.Add(estimate);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.CID = new SelectList(db.customer, "ID", "Name", estimate.CID);
            ViewBag.Barcode = new SelectList(db.inventory, "Barcode", "ProductName", estimate.Barcode);
            return View(estimate);
        }

        // GET: Estimates/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estimate estimate = await db.estimate.FindAsync(id);
            if (estimate == null)
            {
                return HttpNotFound();
            }
            ViewBag.CID = new SelectList(db.customer, "ID", "Name", estimate.CID);
            ViewBag.Barcode = new SelectList(db.inventory, "Barcode", "ProductName", estimate.Barcode);
            return View(estimate);
        }

        // POST: Estimates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Barcode,CID,Qty,Item,BoardRate,Purity,GrossWeight,NetWeight,StoneWeight,UnitPrice,TotalPrice,HSNNO,Taxes,FinalPrice,DateTimeStamp")] Estimate estimate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estimate).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CID = new SelectList(db.customer, "ID", "Name", estimate.CID);
            ViewBag.Barcode = new SelectList(db.inventory, "Barcode", "ProductName", estimate.Barcode);
            return View(estimate);
        }

        // GET: Estimates/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estimate estimate = await db.estimate.FindAsync(id);
            if (estimate == null)
            {
                return HttpNotFound();
            }
            return View(estimate);
        }

        // POST: Estimates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Estimate estimate = await db.estimate.FindAsync(id);
            db.estimate.Remove(estimate);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        List<Customer> cust = new List<Customer>();
        public JsonResult fetchcustomer(string mob)
        {
            List<Customer> cust1 = new List<Customer>();
            int count = int.Parse(TempData.Peek("count").ToString());

            if (count == 0)
            {
                cust = db.customer.ToList();
                TempData["count"] = count++;
            }
            cust1 = cust.Where(e => e.Mobile.StartsWith(mob)).ToList();
            var mobiles = cust.Where(e => e.Mobile.StartsWith(mob)).Select(e => new
            {
                ID = e.ID,
                Text = e.Mobile
            });
            return Json(mobiles, JsonRequestBehavior.AllowGet);
        }
    }
}

