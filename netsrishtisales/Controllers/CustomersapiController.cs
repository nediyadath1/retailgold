﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using netsrishtisales.Models;

namespace netsrishtisales.Controllers
{
    public class CustomersapiController : ApiController
    {
        private SalesContext db = new SalesContext();

        // GET: api/Customersapi
        //public IQueryable<Customer> Getcustomer()
        //{
        //    return db.customer;
        //}

        // GET: api/Customersapi/5
        [ResponseType(typeof(Customer))]
        public async Task<IHttpActionResult> GetCustomer(string cust)
        {
            Int64 mobile = 0;
            bool check = Int64.TryParse(cust, out mobile);
            if (check)
            {

                try
                {
                    List<Customer> cu = db.customer.Where(e => e.Mobile.StartsWith(cust)).ToList();
                    return Ok(cu);
                }
                catch (Exception ex)
                {
                    return NotFound();
                }


            }
            else
            {
                try
                {
                    List<Customer> c = db.customer.Where(e => e.Name.StartsWith(cust)).ToList();
                    return Ok(c);
                }
                catch (Exception ex)
                {
                    return NotFound();
                }

            }




        }

        // PUT: api/Customersapi/5
        [ResponseType(typeof(void))]
        //public async Task<IHttpActionResult> PutCustomer(int id, Customer customer)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != customer.ID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(customer).State = EntityState.Modified;

        //    try
        //    {
        //        await db.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!CustomerExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        // POST: api/Customersapi
        [ResponseType(typeof(Customer))]
        public async Task<IHttpActionResult> PostCustomer(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.customer.Add(customer);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = customer.ID }, customer);
        }

        // DELETE: api/Customersapi/5
        [ResponseType(typeof(Customer))]
        public async Task<IHttpActionResult> DeleteCustomer(int id)
        {
            Customer customer = await db.customer.FindAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            db.customer.Remove(customer);
            await db.SaveChangesAsync();

            return Ok(customer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CustomerExists(int id)
        {
            return db.customer.Count(e => e.ID == id) > 0;
        }
    }
}