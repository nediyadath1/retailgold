﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using netsrishtisales.Models;

namespace netsrishtisales.Controllers
{
    public class EstimatesApiController : ApiController
    {
       
        private SalesContext db = new SalesContext();

        // GET: api/EstimatesApi
        //public IQueryable<Estimate> Getestimate()
        //{
        //    return db.estimate;
        //}

        // GET: api/EstimatesApi/5
        [ResponseType(typeof(Estimate))]
        public async Task<IHttpActionResult> GetEstimate(string id)
        {
            Inventory inventory = new Inventory();
            try
            {
                inventory = await db.inventory.FindAsync(id);
                if (inventory == null)
                {
                    return NotFound();
                }
                //  ProductType ptype = db.producttype.Single(e => e.Purity == inventory.Purity && e.Item == inventory.Item);
                if (inventory.ProductType == "Fixed")
                    return Ok(inventory);
                else
                {
                    return Ok(inventory.ProductType);
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return Ok(message);
            }

        }


        // PUT: api/EstimatesApi/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutEstimate(int id, Estimate estimate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != estimate.ID)
            {
                return BadRequest();
            }

            db.Entry(estimate).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EstimateExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/EstimatesApi
        [ResponseType(typeof(Estimate))]
        public async Task<IHttpActionResult> PostEstimate(Estimate estimate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.estimate.Add(estimate);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = estimate.ID }, estimate);
        }

        // DELETE: api/EstimatesApi/5
        [ResponseType(typeof(Estimate))]
        public async Task<IHttpActionResult> DeleteEstimate(int id)
        {
            Estimate estimate = await db.estimate.FindAsync(id);
            if (estimate == null)
            {
                return NotFound();
            }

            db.estimate.Remove(estimate);
            await db.SaveChangesAsync();

            return Ok(estimate);
        }
        //public async Task<IHttpActionResult> get(float? boardrate, string item)
        //{

        //    ProductType pt = db.producttype.Single(e => e.Item == item && e.BoardRate == boardrate);


        //    return Ok(pt);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EstimateExists(int id)
        {
            return db.estimate.Count(e => e.ID == id) > 0;
        }
    }
}