﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.Ajax.Utilities;
using netsrishtisales.Models;

namespace netsrishtisales.Controllers
{
    [System.Web.Http.RoutePrefix("api/ProductTypes")]

    public class ProductTypesController : ApiController
    {
        private SalesContext db = new SalesContext();

        // GET: api/ProductTypes
        public Inventory Getproducttype(int qty, float? wt, string barcode, string applicabletax)
        {
            Inventory inv = db.inventory.Single(e => e.Barcode == barcode);

            ProductType pt = db.producttype.Single(e => e.Item == inv.Item && e.Purity == inv.Purity);
            Estimate est = new Estimate();
            if (inv.ProductType == "gram")
            {
                inv.UnitPrice = pt.BoardRate * qty * (wt - inv.StoneCharges);

                inv.GrossWeight = wt;
                inv.NetWeight = inv.GrossWeight - inv.StoneCharges;
            }
            else if (inv.ProductType == "Box")
            {
                float up = est.UnitPrice ?? 0;
                float tax = 0F;

                switch (applicabletax)
                {
                    case "SGST":
                        tax = pt.SGST;
                        break;
                    case "UTGST":
                        tax = pt.UTGST;
                        break;
                    case "IGST":
                        tax = pt.IGST;
                        break;
                }
                inv.UnitPrice = pt.BoardRate * qty * wt;
                inv.GrossWeightRemaining = inv.GrossWeightRemaining - wt;
                inv.Qty = inv.Qty - qty;
                if (inv.GrossWeightRemaining == 0 || inv.Qty == 0)
                {
                    inv.available = false;
                }
                inv.GrossWeight = inv.GrossWeight;
                inv.NetWeight = inv.GrossWeight;

            }

            return inv;
        }
        [HttpPost]
        [System.Web.Http.Route("Getdetail")]

        public void Getdetail(customerestimate ce)
        {
            List<Estimate> estlist = new List<Estimate>();
            foreach (Inventory e in ce.estlist)
            {
                Estimate esti = new Estimate();
                esti.CID = ce.custlist[0].ID;
                esti.Barcode = e.Barcode;
                esti.ProductName = e.ProductName;
                esti.Item = e.Item;
                esti.Purity = e.Purity;
                esti.Qty = e.Qty;
                esti.GrossWeight = e.GrossWeight;
                esti.NetWeight = e.NetWeight;
                esti.DateTimeStamp = e.TimeStamp;


                estlist.Add(esti);
                db.Entry(esti).State = EntityState.Added;
                db.SaveChanges();

              

                //db.estimate.Add(e);
                //db.SaveChanges();
            }

            //return Ok() ;
        }




        //// GET: api/ProductTypes/5
        //public async Task<IHttpActionResult> GetProductType(float? boardrate, string item)
        //{
        //    ProductType pt = db.producttype.Single(e => e.BoardRate == boardrate && e.Item == item);


        //    return Ok(pt);
        //}

       // PUT: api/ProductTypes/5
       [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutProductType(int id, ProductType productType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != productType.ID)
            {
                return BadRequest();
            }

            db.Entry(productType).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ProductTypes
        [ResponseType(typeof(ProductType))]
        public async Task<IHttpActionResult> PostProductType(ProductType productType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.producttype.Add(productType);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = productType.ID }, productType);
        }

        // DELETE: api/ProductTypes/5
        [ResponseType(typeof(ProductType))]
        public async Task<IHttpActionResult> DeleteProductType(int id)
        {
            ProductType productType = await db.producttype.FindAsync(id);
            if (productType == null)
            {
                return NotFound();
            }

            db.producttype.Remove(productType);
            await db.SaveChangesAsync();

            return Ok(productType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductTypeExists(int id)
        {
            return db.producttype.Count(e => e.ID == id) > 0;
        }
    }
}